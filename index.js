function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    if(letter.length===1) {
        return sentence.split('').forEach(x => x == letter ? result++ : null)
    } else {
        return undefined
    }
}

/////////////////////////////////

function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    text.toLowerCase();
    for (var i=0; i<text.length; i++) {
      if ( text.indexOf(text[i]) !== text.lastIndexOf(text[i]) ) {
        return false; // repeats
      }                                                                     
    }
  return true;
    
}

////////////////////////////////////////////

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    let discount = price*.20
    let discountedPrice = price-discount

    if(age<13){
        return undefined;
    } else if (age>=13 && age<=21){
        return discountedPrice.toFixed(2).toString()
    } else if (age>=22 && age<=64){
        return price.toFixed(2).toString()
    } else {
        return discountedPrice.toFixed(2).toString()
    }
}

///////////////////////////////

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    let newArr = [];    
     let ilen = items.length
     for(let i = 0; i < ilen; i++){
       if(items[i].stocks === 0){
         newArr.push(items[i].category)
       }
    }
    return [...new Set(newArr)];

}

/////////////////////////////////////////////////

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    var sorted_a = candidateA.concat().sort();
    var sorted_b = candidateB.concat().sort();
    var common = [];
    var a_i = 0;
    var b_i = 0;

    while (a_i < candidateA.length
           && b_i < candidateB.length)
    {
        if (sorted_a[a_i] === sorted_b[b_i]) {
            common.push(sorted_a[a_i]);
            a_i++;
            b_i++;
        }
        else if(sorted_a[a_i] < sorted_b[b_i]) {
            a_i++;
        }
        else {
            b_i++;
        }
    }
    return common;
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};